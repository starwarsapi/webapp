# webapp



## Getting started

This is webapp service used for StarWars API.

Build it using `docker-compose build webapp`.\
Start it using `docker-compose up webapp`.

This service should be placed inside sw-dev-env.